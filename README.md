# Gambler's fallacy simulator

## Tuning the experiment

You have three choice strategies (`<strategy>`) available for your bet:
- random : choose a color randomly (equiprobable) between BLACK and RED
- constant : always choose BLACK color
- least : always choose the color that had been drawn the less from start

You have to choose how many draws you want (`<iteration>`).

You have to set a seed for the random generator (`<seed>`).

## Usage

```
Usage : ./gambler.py <strategy> <iterations> <seed>
	 <strategy> : 'least', 'constant' or 'random' 
	 <iteration>: positive integer
	 <seed>     : anything to control random generator initialization
```

## Examples

### Constant strategy

```
gambler % ./gambler.py constant 1000000 randomseed
Status for simulation:
 - Seed for random generator : randomseed
 - Strategy                  : <function choose_constant at 0x10ad42dc0>
 - Number of draws           : 1000000
 - Number of successes       : 499688 (49.9688%)
```

### Random strategy

```
gambler % ./gambler.py random 1000000 randomseed
Status for simulation:
 - Seed for random generator : randomseed
 - Strategy                  : <function choose_random at 0x108b42e50>
 - Number of draws           : 1000000
 - Number of successes       : 500119 (50.0119%)
```

### Least strategy

```
gambler % ./gambler.py least 1000000 randomseed
Status for simulation:
 - Seed for random generator : randomseed
 - Strategy                  : <function choose_least at 0x10b4e2430>
 - Number of draws           : 1000000
 - Number of successes       : 499709 (49.9709%)
```
