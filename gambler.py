#!/usr/bin/python3

import sys
import random
from enum import Enum

Color = Enum('Color', 'BLACK RED')

def choose_least(counters):
    if counters[Color.BLACK] < counters[Color.RED]:
        return Color.BLACK
    else:
        return Color.RED

def choose_constant(counters):
    return Color.BLACK

def choose_random(counters):
    return random.choice(list(Color))

def draw_a_color():
    return random.choice(list(Color))

STRATEGIES = {
    'least': choose_least,
    'constant': choose_constant,
    'random': choose_random,
}

def usage(msg=None):
    if msg:
        print(msg)
    print('Usage : %s <strategy> <iterations> <seed>' % sys.argv[0])
    print('''\t <strategy> : 'least', 'constant' or 'random' ''')
    print('''\t <iteration>: positive integer''')
    print('''\t <seed>     : anything to control random generator initialization''')
    sys.exit(1)

if len(sys.argv) != 4:
    usage('Error: too many or not enough arguments given')

strategy, iteration, seed = sys.argv[1:]

if not strategy in ['least', 'constant', 'random']:
    usage('Error: illegal value for <strategy>')

strategy = STRATEGIES[strategy]

try:
    iteration = int(iteration)
except:
    usage('Error: Could not convert <iteration> to an integer')

random.seed(seed)

counters = {
    'successes': 0,
    Color.BLACK: 0,
    Color.RED:   0,
}

for draw_count in range(1, iteration + 1):
    choice = strategy(counters)
    draw = draw_a_color()
    counters[draw] += 1
    if choice == draw:
        counters['successes'] += 1
else:
    print('Status for simulation:')
    print(' - Seed for random generator : {}'.format(seed))
    print(' - Strategy                  : {}'.format(strategy))
    print(' - Number of draws           : {:n}'.format(draw_count))
    print(' - Number of successes       : {:n} ({:n}%)'.format(counters['successes'], counters['successes'] / draw_count * 100))
